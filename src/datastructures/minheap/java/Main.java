package datastructures.minheap.java;

public class Main {
    public static void main(String[] args){
        MinHeap<Integer> minHeap = new MinHeap<>();
        System.out.println("size: "+ minHeap.size());
        System.out.println(minHeap);

        minHeap.add(77);
        minHeap.add(55);
        minHeap.add(66);
        minHeap.add(44);
        minHeap.add(10);
        minHeap.add(22);
        minHeap.add(33);
        System.out.println("size: "+ minHeap.size());
        System.out.println("head: " + minHeap.peek());
        System.out.println(minHeap);


        while (!minHeap.isEmpty()){
            System.out.println("head polled: " + minHeap.poll());
        }
    }
}

class MinHeap<E extends Comparable>{
    private final int DEFAULT_INITIAL_CAPACITY = 10;

    private Object[] arr;
    private int size = 0;
    private int capacity;

    public MinHeap(){
        this.capacity = DEFAULT_INITIAL_CAPACITY;
        this.arr = new Object[DEFAULT_INITIAL_CAPACITY];
    }

    public MinHeap(int capacity){
        this.capacity = capacity;
        this.arr = new Object[capacity];
    }

    public int size(){
        return size;
    }

    public boolean isEmpty(){
        return size == 0;
    }

    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (int i=0; i<this.size; i++){
            sb.append(this.arr[i]);
            if (i < this.size-1){
                sb.append(", ");
            }
        }
        sb.append("]");
        return sb.toString();
    }

    public void add(E elem){
        ensureCapacity(this.size + 1);
        this.arr[this.size++] = elem;
        insertBubble();
    }

    public E peek(){
        return (E)this.arr[0];
    }

    public E poll(){
        Object min = this.arr[0];
        this.arr[0] = this.arr[this.size-1];
        this.arr[this.size-1] = null;
        this.size--;
        removeBubble();
        return (E)min;
    }

    private void insertBubble(){
        int indexC = this.size-1;
        int indexP = (indexC - 1)/2;
        while (indexC > 0 && compare(this.arr[indexP], this.arr[indexC]) > 0){
            swap(indexC, indexP);
            indexC = indexP;
            indexP = (indexC - 1)/2;
        }
    }

    private int compare(Object o1, Object o2){
        return ((E)o1).compareTo((E)o2);
    }

    private void removeBubble(){
        int indexP = 0;
        while (indexP < this.size){
            int indexCL = indexP*2 + 1;
            int indexCR = indexP*2 + 2;
            if (indexCL >= this.size && indexCR >= this.size){
                break;
            }
            if (indexCL >= this.size && indexCR < this.size){
                if (compare(this.arr[indexP], this.arr[indexCR]) > 0){
                    swap(indexP, indexCR);
                    indexP = indexCR;
                } else {
                    break;
                }
            }
            if (indexCR >= this.size && indexCL < this.size){
                if (compare(this.arr[indexP], this.arr[indexCL]) > 0){
                    swap(indexP, indexCL);
                    indexP = indexCL;
                } else {
                    break;
                }
            }
            int minChildIndex = compare(this.arr[indexCL], this.arr[indexCR]) <= 0 ? indexCL : indexCR;
            swap(indexP, minChildIndex);
            indexP = minChildIndex;
        }
    }

    private void swap(int i, int j){
        Object temp = this.arr[i];
        this.arr[i] = this.arr[j];
        this.arr[j] = temp;
    }

    private void ensureCapacity(int capacity){
        if (this.capacity >= capacity){
            return;
        }
        this.capacity = Math.max(this.capacity + DEFAULT_INITIAL_CAPACITY, capacity);
        Object[] newArr = new Object[this.capacity];
        System.arraycopy(this.arr, 0, newArr, 0, this.size);
        this.arr = newArr;
    }
}
package datastructures.linkedlist.java;

import java.util.Arrays;
import java.util.Collection;

public class Main {
    public static void main(String args[]){

        LinkedList<Integer> ll = new LinkedList<>();
        System.out.println(ll.size());
        System.out.println(ll);

        ll.add(ll.size(),1);
        ll.add(ll.size(),2);
        ll.add(ll.size(),3);
        System.out.println(ll.size());
        System.out.println(ll);

        ll.add(1,null);
        System.out.println(ll.size());
        System.out.println(ll);
        System.out.println("List contains elem null: " + ll.contains(null));

        ll.addAll(3, Arrays.asList(44,44,44));
        System.out.println(ll.size());
        System.out.println(ll);
        System.out.println("List contains elem 44: " + ll.contains(44));

        ll.remove(null);
        System.out.println(ll.size());
        System.out.println(ll);

        ll.removeAll(Arrays.asList(44, null));
        System.out.println(ll.size());
        System.out.println(ll);

    }
}

class LinkedList<E> {

    private int size = 0;

    private Node<E> first;
    private Node<E> last;

    public LinkedList() {

    }

    public int size() {
        return size;
    }

    public void add(int index, Object o) {
        rangeCheck(index);

        if (this.first == null){ // empty list => index = 0
            this.first = this.last = new Node(null, (E) o, null);
        } else if (this.first.next == null){ // list with one elem where first == last => index in {0, 1}
            if (index == 0){
                this.first = new Node(null, (E) o, this.last);
                this.last.prev = this.first;
            } else {
                this.last = new Node(this.first, (E) o, null);
                this.first.next = this.last;
            }
        } else {
            Node<E> newNode;
            if (index == 0){
                newNode = new Node(null, (E) o, this.first);
                this.first.prev = newNode;
                this.first = newNode;
            } else if (index == this.size) {
                newNode = new Node(this.last, (E) o, null);
                this.last.next = newNode;
                this.last = newNode;
            } else {
                Node<E> temp = this.first.next;
                for (int i=1; i<index; i++){
                    temp = temp.next;
                }
                newNode = new Node(temp.prev, (E) o, temp);
                temp.prev.next = newNode;
                temp.prev = newNode;
            }
        }

        this.size++;
    }

    public void addAll(int index, Collection<? extends E> c){
        rangeCheck(index);
        for (E e:c){
            add(index++, e);
        }
    }

    public boolean contains(Object o){
        Node<E> temp = this.first;
        while (temp != null){
            if (o == null){
                if (temp.data == o) return true;
            } else {
                if (((E)o).equals(temp.data)) return true;
            }
            temp = temp.next;
        }
        return false;
    }

    public boolean removeAll(Collection<? extends E> c){
        int sizeBeforeRemove = this.size;

        Node<E> temp = this.first;
        while (temp != null){
            if (c.contains(temp.data)){
                remove(temp.data);
            }
            temp = temp.next;
        }

        return this.size != sizeBeforeRemove;
    }

    public boolean remove(Object o){
        Node<E> temp = this.first;
        while (temp != null){
            if (o == null){
                if (temp.data == o) {
                    removeNode(temp);
                    return true;
                }
            } else {
                if (temp.data.equals((E)o)) {
                    removeNode(temp);
                    return true;
                }
            }
            temp = temp.next;
        }
        return false;
    }

    public String toString(){
        StringBuffer sb = new StringBuffer();
        sb.append("[");

        Node<E> temp = this.first;
        while (temp != null){
            sb.append(temp.data);
            if (temp.next != null){
                sb.append(", ");
            }
            temp = temp.next;
        }

        sb.append(" ]");
        return sb.toString();
    }

    private void removeNode(Node<E> node){
        if (this.first == this.last){ // one elem in the list
            this.first = this.last = null;
        } else if (node == this.first) {
            this.first = this.first.next;
            node = null;
        } else if (node == this.last){
            this.last = this.last.prev;
            node = null;
        } else {
            node.prev.next = node.next;
            node.next.prev = node.prev;
            node = null;
        }
        this.size--;
    }

    private void rangeCheck(int index) {
        if (index < 0 || index > this.size) {
            throw new IndexOutOfBoundsException("Invalid index for a list with size " + this.size);
        }
    }

    private static class Node<E> {
        E data;

        Node<E> next;
        Node<E> prev;

        public Node(Node<E> prev, E data, Node<E> next) {
            this.prev = prev;
            this.data = data;
            this.next = next;
        }
    }
}

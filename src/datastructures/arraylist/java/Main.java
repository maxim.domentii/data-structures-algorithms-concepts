package datastructures.arraylist.java;

import java.util.Arrays;
import java.util.Collection;

public class Main {
    public static void main(String args[]){
        ArrayList<Integer> al = new ArrayList<>(5);
        al.add(1);
        al.add(2);
        al.add(3);
        al.add(4);
        al.add(5);
        System.out.println(al.size());
        System.out.println(al);

        al.addAll(Arrays.asList(6,7,8));
        System.out.println(al.size());
        System.out.println(al);

        al.addAll(Arrays.asList(9,10,11,12,13,14,15));
        System.out.println(al.size());
        System.out.println(al);

        al.add(14,null);
        System.out.println(al.size());
        System.out.println(al);

        al.addAll(15, Arrays.asList(33, 33, 33));
        System.out.println(al.size());
        System.out.println(al);

        al.remove(18);
        System.out.println(al.size());
        System.out.println(al);

        al.remove(null);
        System.out.println(al.size());
        System.out.println(al);

        al.removeAll(Arrays.asList(33));
        System.out.println(al.size());
        System.out.println(al);

        System.out.println("Contains 1: " + al.contains(1) + " at index: " + al.indexOf(1));

        System.out.println("set index 0 with 11: " + al.set(0,11));

        System.out.println("get from index 0: " + al.get(0));

        System.out.println("Sublist between 0 and 13: " + al.subList(0,13));

        al.clear();
        System.out.println("Clear list and new list is: " + al);
    }
}

class ArrayList<E> {

    private static final int DEFAULT_INITIAL_SIZE = 10;

    private Object[] elements;
    private int size;

    public ArrayList(){
        this.elements = new Object[DEFAULT_INITIAL_SIZE];
        this.size = 0;
    }

    public ArrayList(int initialSize){
        this.elements = new Object[initialSize];
        this.size = 0;
    }

    public int size() {
        return size;
    }

    public boolean add(E e){
        ensureCapacity(this.size + 1);
        this.elements[this.size++] = e;
        return true;
    }

    public boolean add(int index, E e){
        rangeCheck(index);
        ensureCapacity(this.size + 1);
        System.arraycopy(this.elements, index, this.elements, index+1, this.size - index);
        this.elements[index] = e;
        this.size++;
        return true;
    }

    public boolean addAll(Collection<? extends E> c){
        ensureCapacity(this.size + c.size());
        copyRange(this.size, c);
        return true;
    }

    public boolean addAll(int index, Collection<? extends E> c){
        rangeCheck(index);
        ensureCapacity(this.size + c.size());
        System.arraycopy(this.elements, index, this.elements, index+c.size(), this.size - index);
        copyRange(index, c);
        return true;
    }

    public void clear() {
        for (int i=0; i<this.size; i++){
            this.elements[i] = null;
        }
        this.size = 0;
    }

    public boolean contains(Object o){
        return indexOf(o) >= 0;
    }

    public int indexOf(Object o) {
        if (o == null){
            for (int i=0; i<this.size; i++){
                if (this.elements[i]==null){
                    return i;
                }
            }
        } else {
            for (int i=0; i<this.size; i++){
                if ((this.elements[i]).equals(o)){
                    return i;
                }
            }
        }
        return -1;
    }

    public E get(int index) {
        rangeCheck(index);
        return (E) this.elements[index];
    }

    public E set(int index, E e){
        rangeCheck(index);
        E old = (E)this.elements[index];
        this.elements[index] = e;
        return old;
    }

    public E remove(int index){
        rangeCheck(index);
        E removed = (E)this.elements[index];
        System.arraycopy(this.elements, index + 1, this.elements, index, this.size - index - 1);
        this.elements[--this.size] = null;
        return removed;
    }

    public boolean remove(Object o){
        int index = indexOf(o);
        if (index < 0) {
            return false;
        } else {
            System.arraycopy(this.elements, index + 1, this.elements, index, this.size - index - 1);
            this.elements[--this.size] = null;
            return true;
        }
    }

    public boolean removeAll(Collection<?> c){
        boolean modified = false;
        int j = 0;
        for (int i=0; i<this.size; i++){
            if (!c.contains(this.elements[i])){
                this.elements[j++] = this.elements[i];
            }
        }
        if (j != this.size){
            for (int i=j; i<this.size; i++){
                this.elements[i] = null;
            }
            this.size = j;
            modified = true;
        }
        return modified;
    }

    public ArrayList<E> subList(int start, int end){
        if (start < 0 || start >= this.size){
            throw new IndexOutOfBoundsException("Invalid start index!");
        }
        if (end < 0 || end >= this.size){
            throw new IndexOutOfBoundsException("Invalid end index!");
        }
        if (start > end) {
            throw new IllegalArgumentException("start index can't be greater then end index");
        }

        ArrayList<E> subList = new ArrayList(end - start);
        for (int i=start; i<end; i++){
            subList.add((E)this.elements[i]);
        }

        return subList;
    }

    public String toString(){
        StringBuffer sb = new StringBuffer();
        sb.append("[");
        for (int i=0; i<this.size; i++){
            sb.append(" " + this.elements[i]);
            if (i != this.size - 1) {
                sb.append(",");
            }
        }
        sb.append(" ]");
        return sb.toString();
    }

    private void ensureCapacity(int capacity){
        if (elements.length < capacity){
            int newCapacity = Math.max(elements.length + DEFAULT_INITIAL_SIZE, capacity);
            Object[] copy = new Object[newCapacity];
            System.arraycopy(elements, 0, copy, 0, elements.length);
            elements = copy;
        }
    }

    private void rangeCheck(int index) {
        if (index < 0 || index >= this.size){
            throw new IndexOutOfBoundsException("Invalid index!");
        }
    }

    private void copyRange(int index, Collection<? extends E> c){
        this.size += c.size();
        for (E e:c){
            this.elements[index++] = e;
        }
    }
}
public class ArrayList<E> {

    private static final int DEFAULT_INITIAL_SIZE = 10;

    private Object[] elements;
    private int size;

    public ArrayList<E>(){
        this.elements = new Object[DEFAULT_INITIAL_SIZE];
        this.size = 0;
    }

    public ArrayList<E>(int initialSize){
        this.elements = new Object[initialSize];
        this.size = 0
    }

    public int size() {
        return size;
    }

    public boolean add(E e){
        ensureCapacity(this.size + 1);
        this.elements[this.size++] = e;
        return true;
    }

    public boolean add(int index, E e){
        rangeCheck(index);
        ensureCapacity(this.size + 1);
        System.arraycopy(this.elements, index, this.elements, index+1, this.size - index);
        this.elements[this.size++] = e;
        return true;
    }

    public boolean addAll(Collection<? extends E> c){
        ensureCapacity(this.size + c.size());
        copyRange(this.size, c);
        return true;
    }

    public boolean addAll(int index, Collection<? extends E> c){
        rangeCheck(index);
        ensureCapacity(this.size + c.size());
        System.arraycopy(this.elements, index, this.elements, index+c.size(), this.size - index);
        copyRange(index, c);
        return true;
    }

    public void clear() {
        for (int i=0; i<this.size; i++){
            this.elements[i] = null;
        }
    }

    public boolean contains(Object o){
        return indexOf(o) >= 0;
    }

    public int indexOf(Object o) {
        if (o == null){
            for (int i=0; i<this.size; i++){
                if (this.elements[i]==null){
                    return i;
                }
            }
        } else {
            for (int i=0; i<this.size; i++){
                if (this.elements.equals(o)){
                    return i;
                }
            }
        }
        return -1;
    }

    public E get(int index) {
        rangeCheck(index);
        return this.elements[index];
    }

    pubic E set(int index, E e){
        rangeCheck(index);
        E old = this.elements[index];
        this.elements[index] = e;
        return old;
    }

    public E remove(int index){
        rangeCheck(index);
        E removed = this.elements[index];
        System.arraycopy(this.elements, index + 1, this.elements, index, this.size - index - 1);
        this.elements[--this.size] = null;
        return removed;
    }

    public boolean remove(Object o){
        int index = indexOf(o);
        if (index < 0) {
            return false;
        } else {
            System.arraycopy(this.elements, index + 1, this.elements, index, this.size - index - 1);
            this.elements[--this.size] = null;
        }

    }

    public boolean removeAll(Collection<?> c){
        boolean modified = false;
        for (int i=0; i<this.size; i++){
            if (c.contains(this.elements[i])){
                remove(i);
                modified = true;
            }
        }
        return modified;
    }

    public ArrayList<E> subList(int start, int end){
        if (start < 0 || start >= this.size){
            throw new IndexOutOfBoundException("Invalid start index!");
        }
        if (end < 0 || end >= this.size){
            throw new IndexOutOfBoundException("Invalid end index!");
        }
        if (start > end) {
            throw new InvalidArgumentException("start index can't be greater then end index");
        }

        ArrayList<E> subList = new ArrayList(end - start);
        for (int i=start; i<end; i++){
            subList.add(this.elements[i]);
        }

        return subList;
    }

    public String toString(){
        StringBuffer sb = new StringBuffer();
        sb.append("[");
        for (int i=0; i<this.size; i++){
            sb.append(" " + this.elements[i]);
            if (i == this.size - 1) {
                sb.append(",");
            }
        }
        sb.append("]");
    }

    private void ensureCapacity(int capacity){
        if (elements.length < capacity){
            int newCapacity = Math.max(elements.length + DEFAULT_INITIAL_SIZE, capacity);
            Object[] copy = new Object[newCapacity];
            System.arraycopy(elements, 0, copy, 0, elements.length);
            elements = copy;
        }
    }

    private void rangeCheck(int index) {
        if (index < 0 || index >= this.size){
            throw new IndexOutOfBoundException("Invalid index!");
        }
    }

    private void copyRange(int index, Collection<? extends E> c){
        for (E e:c){
            this.elements[index++] = e;
        }
    }
}
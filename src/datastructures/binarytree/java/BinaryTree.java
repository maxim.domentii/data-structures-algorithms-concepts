package datastructures.binarytree.java;

import java.util.LinkedList;
import java.util.Queue;

public class BinaryTree<K extends Comparable,V> {
    private int size = 0;
    public Entry<K,V> root;

    public BinaryTree() {}

    public void add(K key, V value){
        checkKeyForNull(key);
        this.root = add(key, value, this.root);
        this.size++;
    }

    public int size(){
        return size;
    }

    public String toString(){
        // return tree as string in level order (BFS)
        if (size==0) return "";

        StringBuilder sb = new StringBuilder();
        sb.append("[ ");

        Queue<Entry<K,V>> q = new LinkedList<>();
        q.add(this.root);
        Entry<K,V> node;
        while (!q.isEmpty()){
            node = q.poll();
            sb.append(node + " ");
            if (node.left != null){
                q.add(node.left);
            }
            if (node.right != null){
                q.add(node.right);
            }
        }

        sb.append("]");
        return sb.toString();
    }

    private Entry<K,V> rotateLeft(Entry<K,V> node){
        Entry<K,V> right = node.right;
        Entry<K,V> temp = right.left;

        right.left = node;
        node.right = temp;

        node.height = Math.max(height(node.left), height(node.right)) + 1;
        right.height = Math.max(height(right.left), height(right.right)) + 1;

        return right;
    }

    private Entry<K,V> rotateRight(Entry<K,V> node){
        Entry<K,V> left = node.left;
        Entry<K,V> temp = left.right;

        left.right = node;
        node.left = temp;

        node.height = Math.max(height(node.left), height(node.right)) + 1;
        left.height = Math.max(height(left.left), height(left.right)) + 1;

        return left;
    }

    private Entry<K,V> add(K key, V value, Entry<K,V> node){
        if (node == null){
            return new Entry<>(1, key, value, null, null);
        }

        if (key.compareTo(node.key) < 0){
            node.left = add(key, value, node.left);
        } else if (key.compareTo(node.key) > 0) {
            node.right = add(key, value, node.right);
        } else {
            node.value = value;
            return node;
        }

        node.height = Math.max(height(node.left), height(node.right)) + 1;

        int balance = getBalance(node);
        if (balance > 1 && key.compareTo(node.left.key) < 0){ // left-left case
            return rotateRight(node);
        }
        if (balance < -1 && key.compareTo(node.right.key) > 0){ // right-right case
            return rotateLeft(node);
        }
        if (balance > 1 && key.compareTo(node.left.key) > 0){ // left-right case
            node.left = rotateLeft(node.left);
            return rotateRight(node);
        }
        if (balance < -1 && key.compareTo(node.right.key) < 0){ // right-left case
            node.right = rotateRight(node.right);
            return rotateLeft(node);
        }

        return node;
    }

    private int height(Entry<K,V> node){
        if (node == null){
            return 0;
        }
        return node.height;
    }

    private int getBalance(Entry<K,V> node){
        if (node == null){
            return 0;
        }
        return height(node.left) - height(node.right);
    }

    private void checkKeyForNull(K key){
        if (key == null){
            throw new IllegalArgumentException("key can't be null!");
        }
    }

    public static class Entry<K,V>{
        int height;
        K key;
        V value;
        public Entry<K,V> right;
        public Entry<K,V> left;

        public Entry(int height, K key, V value, Entry<K,V> right, Entry<K,V> left){
            this.height = height;
            this.key = key;
            this.value = value;
            this.right = right;
            this.left = left;
        }

        public String toString(){
            return  "{ " + key + "=" + value + "(" + height + ") }";
        }
    }
}


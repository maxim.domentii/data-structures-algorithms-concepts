package datastructures.binarytree.java;

public class Main {
    public static void main(String args[]){
        BinaryTree<Integer, String> bt = new BinaryTree<>();
        System.out.println(bt.size());
        System.out.println(bt);

        bt.add(10, "");
        System.out.println(bt.size());
        System.out.println(bt);

        bt.add(15, "");
        System.out.println(bt.size());
        System.out.println(bt);

        bt.add(5, "");
        System.out.println(bt.size());
        System.out.println(bt);

        bt.add(3, "");
        System.out.println(bt.size());
        System.out.println(bt);

        bt.add(1, "");
        System.out.println(bt.size());
        System.out.println(bt);

        bt.add(4, "");
        System.out.println(bt.size());
        System.out.println(bt);
    }
}
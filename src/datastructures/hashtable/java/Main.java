package datastructures.hashtable.java;

public class Main {
    public static void main(String args[]){
        Hashtable<Integer, String> ht = new Hashtable<>(5);
        System.out.println(ht.size());
        System.out.println(ht);

        ht.put(1, "a");
        ht.put(2, "null");
        ht.put(3, null);
        ht.put(4, "a");
        ht.put(5, "b");
        System.out.println(ht.size());
        System.out.println(ht);

        ht.put(5, "c");
        System.out.println(ht.size());
        System.out.println(ht);

        System.out.println("Hashtable contains key 1: " + ht.containsKey(1));

        ht.remove(1);
        System.out.println(ht.size());
        System.out.println(ht);

        System.out.println("After removed key 1 Hashtable contains key 1: " + ht.containsKey(1));

        System.out.println("Value under key 5 is: " + ht.get(5));
    }
}

class Hashtable<K,V> {
    private int size = 0;
    private float loadFactor = 0.75f;
    private int capacity;
    private int threshold;

    private Entry<?,?>[] table;

    public Hashtable(int initialCapacity){
        this.capacity =  initialCapacity;
        this.threshold = (int)(this.capacity * this.loadFactor);
        this.table = new Entry<?,?>[this.capacity];
    }

    public Hashtable() {
        this(10);
    }

    public int size(){
        return size;
    }

    public V put(K key, V value){
        Entry<K,V> entry = (Entry<K, V>) getEntry(key);
        if (entry != null){
            entry.value = value;
        } else{
            addEntry(key, value);
        }
        return value;
    }

    public V get(K key){
        Entry<K,V> entry = (Entry<K, V>) getEntry(key);
        return entry != null ? entry.value : null;
    }

    public boolean containsKey(K key){
        return getEntry(key) != null;
    }

    public boolean remove(K key){
        Entry<K,V> entry = (Entry<K, V>) getEntry(key);
        if (entry == null){
            return false;
        } else {
            this.table[getBucketIndex(key)] = removeEntryFromBucket(this.table[getBucketIndex(key)], entry);
            return true;
        }
    }

    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("{\n");

        for (int i=0; i<this.capacity; i++){
            sb.append(i + ": ");
            Entry<K,V> temp = (Entry<K, V>) table[i];
            while(temp != null){
                sb.append(temp.key + "=" + temp.value + " ");
                temp = temp.next;
            }
            sb.append("\n");
        }

        sb.append("}");
        return sb.toString();
    }

    private void rehash(){
        this.capacity = this.capacity * 2;
        this.threshold = (int)(this.capacity * this.loadFactor);

        Entry<?,?>[] newTable = new Entry<?,?>[this.capacity];
        for(int i=0; i<this.table.length; i++){
            Entry<K,V> temp = (Entry<K, V>) this.table[i];
            while(temp != null){
                int newBucketIndex = getBucketIndex(temp.key);
                newTable[newBucketIndex] = addEntryInBucket(newTable[newBucketIndex], temp.key, temp.value);
                temp = temp.next;
            }
        }
        this.table = newTable;

    }

    private void addEntry(K key, V value){
        if (this.size >= this.threshold){
            rehash();
        }

        this.table[getBucketIndex(key)] = addEntryInBucket(this.table[getBucketIndex(key)], key, value);
        this.size++;
    }

    private Entry<?,?> addEntryInBucket(Entry<?,?> bucket, K key, V value){
        if (bucket == null){
            bucket = new Entry<>(key, value, null);
        } else {
            Entry<K,V> temp = (Entry<K, V>) bucket;
            while(temp.next != null){
                temp = temp.next;
            }
            temp.next = new Entry<>(key, value, null);
        }
        return bucket;
    }

    private Entry<?,?> removeEntryFromBucket(Entry<?,?> bucket, Entry<K,V> entry){
        if (bucket == entry){
            bucket = bucket.next;
            entry = null;
            return bucket;
        }

        Entry<K,V> temp = (Entry<K, V>) bucket;
        while(temp.next != null){
            if (temp.next == entry){
                temp.next = temp.next.next;
                entry = null;
            } else {
                temp = temp.next;
            }
        }
        return bucket;
    }

    private int getBucketIndex(K key){
        int hash = key.hashCode();
        return hash % this.capacity;
    }

    private Entry<?,?> getEntry(K key){
        if (key == null){
            throw new NullPointerException("key can't be null!");
        }

        Entry<?,?> temp = this.table[getBucketIndex(key)];
        while(temp != null){
            if (temp.key.equals(key)) return temp;
            temp = temp.next;
        }

        return null;
    }

    private static class Entry<K,V> {
        final K key;
        V value;
        Entry<K,V> next;

        public Entry(K key, V value, Entry<K,V> next){
            this.key = key;
            this.value = value;
            this.next = next;
        }
    }

}


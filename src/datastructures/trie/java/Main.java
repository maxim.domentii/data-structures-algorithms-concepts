package datastructures.trie.java;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    private static Trie trie = new Trie();

    public static void writeText(){

        Scanner sc = new Scanner(System.in);
        while (!sc.hasNext("\\n")) {
            String s = sc.next();
            trie.add(s);
            System.out.println(trie.getFirstSuggestions(s));
        }
    }

    public static void main(String[] args){

        writeText();
        /*Trie t = new Trie();
        t.add("b");
        t.add("be");
        t.add("bec");
        t.add("ba");
        t.add("bac");
        t.add("a");
        t.add("al");
        t.add("ala");
        t.add("ac");
        t.add("ace");
        t.add("ace");
        System.out.println(t);
        System.out.println(t.getFirstSuggestions("a"));
        System.out.println(t.getFirstSuggestions("b"));*/
    }
}

class Trie {

    private Node root;

    public Trie(){
        this.root = new Node();
    }

    public void add(String s){
        String sLowerCase = s.toLowerCase();

        Node temp = this.root;
        for (int i=0; i<sLowerCase.length(); i++){
            char c = sLowerCase.charAt(i);
            int index = c - 'a';
            if (temp.children[index] != null){
                temp = temp.children[index];
            } else {
                Node node = new Node();
                temp.children[index] = node;
                temp = node;
            }
        }
        temp.isLeaf = true;
    }

    public boolean contains(String s){
        Node node = searchNode(s);
        if (node != null && node.isLeaf){
            return true;
        }
        return false;
    }

    public List<String> getFirstSuggestions(String s){
        List<String> result = new ArrayList<>();
        Node node = searchNode(s);
        getWords(node, new StringBuilder(), result, 3);
        for (int i=0; i< result.size(); i++){
            result.set(i, s+result.get(i));
        }
        return result;
    }

    public String toString(){
        List<String> result = new ArrayList<>();
        getWords(this.root, new StringBuilder(), result, Integer.MAX_VALUE);
        return result.toString();
    }

    private void getWords(Node node, StringBuilder sb, List<String> result, int resultLimit){
        String temp = sb.toString();
        if (result.size() >= resultLimit){
            return;
        }
        for (int i=0; i<node.children.length; i++){
            sb = new StringBuilder(temp);
            if (node.children[i] != null){
                sb.append((char)('a' + i));
                if (node.children[i].isLeaf){
                    result.add(sb.toString());
                }
                if (result.size() >= resultLimit){
                    break;
                }
                getWords(node.children[i], sb, result, resultLimit);
            }
        }
    }

    private Node searchNode(String s){
        if (s.isEmpty()) {
            return null;
        }

        Node temp = this.root;
        for(int i=0; i<s.length(); i++){
            char c = s.charAt(i);
            int index = c - 'a';
            if (temp.children[index] == null){
                return null;
            } else {
                temp = temp.children[index];
            }
        }
        return temp;
    }

    private static class Node {
        Node[] children;
        boolean isLeaf;

        public Node(){
            this.children = new Node[26];
        }
    }
}

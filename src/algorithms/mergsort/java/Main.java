package algorithms.mergsort.java;

import java.util.Arrays;

public class Main {

    public static void main(String[] args){
        Integer[] arr = new Integer[]{10,9,8,7,6,5,4,3,2,1};
        System.out.println("initial: " + Arrays.toString(arr));
        System.out.println("merge sorted: " + Arrays.toString(MergeSort.mergeSort(arr)));

        Integer[] arr2 = new Integer[]{1,3,5,7,9,2,4,6,8,10};
        System.out.println("initial: " + Arrays.toString(arr2));
        System.out.println("merge sorted: " + Arrays.toString(MergeSort.mergeSort(arr2)));
    }
}

class MergeSort {

    public static Object[] mergeSort(Object[] arr){
        return recursiveMergeSort(arr);
    }

    private static Object[] recursiveMergeSort(Object[] arr){
        if (arr.length < 2){
            return arr;
        }
        int mid = arr.length/2;

        Object[] left = new Object[mid];
        Object[] right = new Object[arr.length-mid];

        System.arraycopy(arr, 0, left, 0, mid);
        System.arraycopy(arr, mid, right, 0, arr.length-mid);

        left = recursiveMergeSort(left);
        right = recursiveMergeSort(right);

        return merge(left, right);
    }

    private static Object[] merge(Object[] left, Object[] right){
        Object[] result = new Object[left.length + right.length];

        int f=0;
        int r=0;
        int i=0;
        while (f < left.length && r < right.length){
            if (((Comparable)left[f]).compareTo(right[r]) <= 0){
                result[i++] = left[f++];
            } else {
                result[i++] = right[r++];
            }
        }
        while (f < left.length){
            result[i++] = left[f++];
        }
        while (r < right.length){
            result[i++] = right[r++];
        }
        return result;
    }
}

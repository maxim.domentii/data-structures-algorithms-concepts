package algorithms.dfs.java;

import datastructures.binarytree.java.BinaryTree;

import java.util.Stack;

public class Main {
    public static void main(String args[]){
        BinaryTree<Integer, String> tree = new BinaryTree<>();
        tree.add(10, "");
        tree.add(5, "");
        tree.add(15, "");
        tree.add(3, "");
        tree.add(7, "");
        tree.add(13, "");
        tree.add(17, "");
        System.out.println("iterative DFS (with Stack): " + DFS.iterativeDfs(tree));
        System.out.println("pre DFS: " + DFS.preDfs(tree));
        System.out.println("in DFS: " + DFS.inDfs(tree));
        System.out.println("post DFS: " + DFS.postDfs(tree));
    }
}

class DFS {
    public static String iterativeDfs(BinaryTree tree){
        StringBuilder sb = new StringBuilder();

        BinaryTree.Entry root = tree.root;
        Stack s = new Stack();
        s.push(root);
        while(!s.isEmpty()){
            BinaryTree.Entry node = (BinaryTree.Entry)s.pop();
            sb.append(node);
            if (node.left != null){
                s.push(node.left);
            }
            if (node.right != null){
                s.push(node.right);
            }
        }
        return sb.toString();
    }

    public static String preDfs(BinaryTree tree){
        StringBuilder sb = new StringBuilder();
        preDfs(tree.root, sb);
        return sb.toString();
    }

    private static void preDfs(BinaryTree.Entry node, StringBuilder sb){
        if (node == null){
            return;
        }
        preDfs(node.left, sb);
        sb.append(node + " ");
        preDfs(node.right, sb);
    }

    public static String inDfs(BinaryTree tree){
        StringBuilder sb = new StringBuilder();
        inDfs(tree.root, sb);
        return sb.toString();
    }

    private static void inDfs(BinaryTree.Entry node, StringBuilder sb){
        if (node == null){
            return;
        }
        sb.append(node + " ");
        inDfs(node.left, sb);
        inDfs(node.right, sb);
    }

    public static String postDfs(BinaryTree tree){
        StringBuilder sb = new StringBuilder();
        postDfs(tree.root, sb);
        return sb.toString();
    }

    private static void postDfs(BinaryTree.Entry node, StringBuilder sb){
        if (node == null){
            return;
        }
        postDfs(node.left, sb);
        postDfs(node.right, sb);
        sb.append(node + " ");
    }
}
package algorithms.heapsort.txt;

import java.util.Arrays;

public class Main {
    public static void main(String[] args){
        Integer[] arr = new Integer[]{10,9,8,7,6,5,4,3,2,1,20,19,18,17,16,15,14,13,12,11};
        System.out.println("initial: " + Arrays.toString(arr));
        HeapSort.heapSort(arr);
        System.out.println("heap sorted: " + Arrays.toString(arr));

        Integer[] arr2 = new Integer[]{1,3,5,7,9,2,4,6,8,10,20,19,18,17,16,15,14,13,12,11};
        System.out.println("initial: " + Arrays.toString(arr2));
        HeapSort.heapSort(arr2);
        System.out.println("heap sorted: " + Arrays.toString(arr2));
    }
}

class HeapSort {

    public static void heapSort(Object[] arr){
        heapify(arr);

        int last = arr.length -1;
        while (last > 0){
            swap(arr, 0, last);
            downShift(arr, last);
            last--;
        }
    }

    private static void downShift(Object[] arr, int end){
        int parentIndex = 0;
        int leftChildIndex = 2 * parentIndex + 1;
        int rightChildIndex = 2 * parentIndex + 2;
        if (leftChildIndex >= end){
            return;
        }

        int maxChildIndex = rightChildIndex < end ? maxIndex(arr, leftChildIndex, rightChildIndex) : leftChildIndex;
        while (((Comparable)arr[parentIndex]).compareTo(arr[maxChildIndex]) < 0){
            swap(arr, parentIndex, maxChildIndex);

            parentIndex = maxChildIndex;
            leftChildIndex = 2 * parentIndex + 1;
            rightChildIndex = 2 * parentIndex + 2;
            if (leftChildIndex >= end){
                break;
            }
            maxChildIndex = rightChildIndex < end ? maxIndex(arr, leftChildIndex, rightChildIndex) : leftChildIndex;
        }

    }

    private static void heapify(Object[] arr){
        int i = arr.length - 1;
        while (i > 0){
            int childIndex = i;
            int parentIndex = (childIndex - 1)/2;
            while (((Comparable)arr[parentIndex]).compareTo(arr[childIndex]) < 0){
                swap(arr, parentIndex, childIndex);

                childIndex = parentIndex;
                parentIndex = (childIndex - 1)/2;
            }
            i--;
        }
    }

    private static void swap(Object[] arr, int i, int j){
        Object temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    private static int maxIndex(Object[] arr, int i, int j){
        return ((Comparable)arr[i]).compareTo((Comparable)arr[j]) >= 0 ? i : j;
    }

}

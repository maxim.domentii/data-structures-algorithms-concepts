package algorithms.bucketsort.java;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.TreeMap;

public class Main {
    public static void main(String[] args){
        int[] arr = new int[]{10,9,8,7,6,5,4,3,2,1,20,19,18,17,16,15,14,13,12,11};
        System.out.println("initial: " + Arrays.toString(arr));
        System.out.println("bucket sorted: " + Arrays.toString(BucketSort.bucketSort(arr)));

        int[] arr2 = new int[]{1,3,5,7,9,2,4,6,8,10,20,19,18,17,16,15,14,13,12,11};
        System.out.println("initial: " + Arrays.toString(arr2));
        System.out.println("bucket sorted: " + Arrays.toString(BucketSort.bucketSort(arr2)));
    }
}

class BucketSort {

    public static int[] bucketSort(int[] arr){
        int noOfBuckets = arr.length;
        List<TreeMap<Integer, String>> treeArr = new ArrayList<>(noOfBuckets);
        for (int i=0; i<noOfBuckets; i++){
            treeArr.add(new TreeMap<>());
        }

        int min = min(arr);
        int max = max(arr);
        int elemPerBucket = (int)Math.ceil((double)(max - min)/noOfBuckets);
        for (int i=0; i<arr.length; i++){
            int bucketIndex = (arr[i] - min)/elemPerBucket;
            treeArr.get(bucketIndex).put(arr[i], "");
        }

        int[] result = new int[arr.length];
        int j=0;
        for (int i=0; i<noOfBuckets; i++){
            for (Integer elem : treeArr.get(i).keySet()){
                result[j++] = elem;
            }
        }
        return result;
    }

    private static int min(int[] arr){
        if (arr == null || arr.length == 0){
            return Integer.MIN_VALUE;
        }

        int min = arr[0];
        for (int i=1; i<arr.length; i++){
            if (arr[i] < min){
                min = arr[i];
            }
        }

        return min;
    }

    private static int max(int[] arr){
        if (arr == null || arr.length == 0){
            return Integer.MAX_VALUE;
        }

        int max = arr[0];
        for (int i=1; i<arr.length; i++){
            if (arr[i] > max){
                max = arr[i];
            }
        }

        return max;
    }

}

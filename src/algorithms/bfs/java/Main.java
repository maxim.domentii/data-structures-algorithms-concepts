package algorithms.bfs.java;

import datastructures.binarytree.java.BinaryTree;

import java.util.LinkedList;
import java.util.Queue;

public class Main {
    public static void main(String args[]){
        BinaryTree<Integer, String> tree = new BinaryTree<>();
        tree.add(1, "");
        tree.add(2, "");
        System.out.println(BFS.bfs(tree));

        tree.add(3, "");
        System.out.println(BFS.bfs(tree));

        tree.add(0, "");
        tree.add(4, "");
        System.out.println(BFS.bfs(tree));
    }
}

class BFS {

    public static String bfs(BinaryTree tree){
        StringBuilder sb = new StringBuilder();

        BinaryTree.Entry root = tree.root;
        Queue q = new LinkedList<>();
        q.add(root);
        while(!q.isEmpty()){
            BinaryTree.Entry node = (BinaryTree.Entry) q.poll();
            sb.append(node + " ");
            if (node.left != null){
                q.add(node.left);
            }
            if (node.right != null){
                q.add(node.right);
            }
        }
        return sb.toString();
    }
}

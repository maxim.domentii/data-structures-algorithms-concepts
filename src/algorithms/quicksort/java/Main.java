package algorithms.quicksort.java;

import java.util.Arrays;

public class Main {
    public static void main(String[] args){
        Integer[] arr = new Integer[]{10,9,8,7,6,5,4,3,2,1};
        System.out.println("initial: " + Arrays.toString(arr));
        QuickSort.quickSort(arr);
        System.out.println("quick sorted: " + Arrays.toString(arr));

        Integer[] arr2 = new Integer[]{1,3,5,7,9,2,4,6,8,10};
        System.out.println("initial: " + Arrays.toString(arr2));
        QuickSort.quickSort(arr2);
        System.out.println("quick sorted: " + Arrays.toString(arr2));

    }
}

class QuickSort {

    public static void quickSort(Object[] arr){
        recursiveQuickSort(arr, 0, arr.length-1);
    }

    private static void recursiveQuickSort(Object[] arr, int start, int end){
        if (start < end){
            int p = partition(arr, start, end);
            recursiveQuickSort(arr, start, p);
            recursiveQuickSort(arr, p+1, end);
        }
    }

    private static int partition(Object[] arr, int start, int end){
        Object pivot = arr[(start + end)/2];
        int i=start;
        int j=end;
        while(true){
            while(((Comparable)arr[i]).compareTo((Comparable)pivot) < 0){
                i++;
            }
            while(((Comparable)arr[j]).compareTo((Comparable)pivot) > 0){
                j--;
            }
            if (i>=j){
                return j;
            }
            Object temp = arr[i];
            arr[i] = arr[j];
            arr[j] = temp;
        }
    }
}
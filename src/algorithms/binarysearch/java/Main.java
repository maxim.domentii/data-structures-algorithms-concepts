package algorithms.binarysearch.java;

public class Main {
    public static void main(String args[]){
        Integer[] arr = new Integer[]{1,2,3,4,5,6,7,8};
        System.out.println(BinarySearch.binarySearch(arr, 4));
    }
}

class BinarySearch {
    public static <T extends Comparable> int binarySearch(T[] arr, T elem){
        return binarySearch(arr, elem, 0, arr.length);
    }

    private static <T extends Comparable> int binarySearch(T[] arr, T elem, int start, int end){
        int mid = (end - start) / 2 + start;
        if (elem.compareTo(arr[mid]) != 0 && (end - start) < 2){
            return -1;
        }
        if (elem.compareTo(arr[mid]) < 0){
            return binarySearch(arr, elem, start, mid);
        }
        if (elem.compareTo(arr[mid]) > 0){
            return binarySearch(arr, elem, mid, end);
        }
        return mid;
    }
}

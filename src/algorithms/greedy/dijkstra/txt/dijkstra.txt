class Dijkstra {
    private int[][] graph;

    public Dijkstra(int[][] graph){
        this.graph = graph;
    }

    public List<Integer> shortestPath1(int sourceNode, int destinationNode){
        List<Integer> path = new ArrayList<>();
        path.add(sourceNode);
        if (sourceNode == destinationNode){
            return path;
        }

        int nextNode = getClosestNeighborhood(sourceNode, path);
        if (nextNode == -1){
            throw new RuntimeException("No path from source to destination");
        }
        path.addAll(shortestPath1(nextNode, destinationNode));
    }

    public List<Integer> shortestPath2(int sourceNode, int destinationNode){
        List<Integer> path = new ArrayList<>();
        buildShortestPath(path, sourceNode, destinationNode);
        return path;
    }

    public int getClosestNeighborhood(int sourceNode, List<Integer> visitedNodes){
        int closest = -1;
        int distance = Integer.MAX_VALUE;
        for (int i=1; i<graph.length; i++){
            if (i != sourceNode && !visitedNodes.contains(i) && graph[sourceNode][i] < distance){
                distance = graph[sourceNode][i];
                closest = i;
            }
        }
        return closest;
    }

    private void buildShortestPath(List<Integer> path, int sourceNode, int destinationNode){
        path.add(sourceNode);
        if (sourceNode == destinationNode){
            return path;
        }
        int nextNode = getClosestNeighborhood(sourceNode, path);
        if (nextNode == -1){
            throw new RuntimeException("No path from source to destination");
        }
        buildShortestPath(path, nextNode, destinationNode);
    }
}
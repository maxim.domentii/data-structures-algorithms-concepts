package algorithms.greedy.dijkstra.java;

import java.util.*;

public class Main {
    public static void main(String[] args){
        Dijkstra dijkstra = new Dijkstra(new int[][]{
                {0,0,0,0,0,0,0},
                {0,0,7,9,0,0,14},
                {0,7,0,10,15,0,0},
                {0,9,10,0,11,0,2},
                {0,0,15,11,0,6,0},
                {0,0,0,0,6,0,9},
                {0,14,0,2,0,9,0}
        });

        System.out.println(dijkstra.shortestPath(1,5));
        System.out.println(dijkstra.shortestDistance(1,5));
    }
}

class Dijkstra {
    private int[][] graph;

    public Dijkstra(int[][] graph){
        this.graph = graph;
    }

    public List<Integer> shortestPath(int sourceNode, int destinationNode){
        List<Integer> path = new ArrayList<>();
        int[][] shortestPaths = shortestPaths(sourceNode);
        int[] dist = shortestPaths[0];
        int[] prev = shortestPaths[1];
        int target = destinationNode;
        if (prev[target] > -1 || target == sourceNode){
            while (target > -1){
                path.add(target);
                target = prev[target];
            }
        }
        return path;
    }

    public int shortestDistance(int sourceNode, int destinationNode){
        int[][] shortestPaths = shortestPaths(sourceNode);
        return shortestPaths[0][destinationNode];
    }

    private int[][] shortestPaths(int sourceNode){
        int[] dist = new int[graph.length];
        Arrays.fill(dist, Integer.MAX_VALUE);
        dist[sourceNode] = 0;

        int[] prev = new int[graph.length];
        Arrays.fill(prev, -1);

        Set<Integer> unvisited  = new TreeSet<>();
        for (int i=1; i<graph.length; i++){
            unvisited.add(i);
        }

        while(!unvisited.isEmpty()){
            int current = getClosestNeighborhood(unvisited, dist);
            unvisited.remove(current);
            List<Integer> unvisitedNeighborhoods = getUnvisitedNeighborhoods(current, unvisited);
            for (int i : unvisitedNeighborhoods){
                int newD = dist[current] + graph[current][i];
                if (newD < dist[i]){
                    dist[i] = newD;
                    prev[i] = current;
                }
            }
        }

        return new int[][]{dist, prev};
    }

    private List<Integer> getUnvisitedNeighborhoods(int node, Set<Integer> unvisited){
        List<Integer> neighborhoods = new ArrayList<>();
        for (int i=1; i<graph.length; i++){
            if (graph[node][i] != 0 && unvisited.contains(i)){
                neighborhoods.add(i);
            }
        }
        return neighborhoods;
    }

    private int getClosestNeighborhood(Set<Integer> unvisited, int[] dist) {
        int minDistance = Integer.MAX_VALUE;
        int neighborhood = -1;
        for (int i : unvisited){
            if (dist[i] < minDistance){
                minDistance = dist[i];
                neighborhood = i;
            }
        }
        return neighborhood;
    }
}

package algorithms.radixsort.java;

import java.util.Arrays;

public class Main {
    public static void main(String[] args){
        int arr[] = {170, 45, 75, 90, 802, 24, 2, 66};
        System.out.println(Arrays.toString(arr));
        RadixSort.radixSort(arr);
        System.out.println(Arrays.toString(arr));
    }
}

class RadixSort {

    public static void radixSort(int[] arr){
        int max = max(arr);

        for (int exp=1; max/exp>0; exp *= 10){
            countingSort(arr, exp);
        }
    }

    private static int max(int[] arr){
        if (arr == null || arr.length == 0){
            return Integer.MAX_VALUE;
        }
        int max = arr[0];
        for (int i=1;i<arr.length; i++){
            if (arr[i] > max){
                max = arr[i];
            }
        }
        return max;
    }

    public static void countingSort(int arr[], int exp){
        int[] output = new int[arr.length];
        int[] count = new int[10];
        Arrays.fill(count, 0);

        // count occurrences
        for (int i=0; i<arr.length; i++){
            count[arr[i]/exp % 10]++;
        }

        // determine position in range
        for (int i=1; i<10; i++){
            count[i] += count[i-1];
        }

        // build output sorted array based on positions form count array
        for (int i=arr.length-1; i>=0; i--){
            output[count[arr[i]/exp % 10]-1] = arr[i];
            count[arr[i]/exp % 10]--;
        }

        for (int i=0; i<arr.length; i++){
            arr[i] = output[i];
        }
    }

}

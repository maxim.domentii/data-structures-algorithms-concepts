package algorithms.dnf.java;

import java.util.Arrays;

public class Main {
    public static void main(String[] args){
        int[] arr = new int[]{2,1,0,2,2,0,0,1};
        System.out.println("initial arr: " + Arrays.toString(arr));

        Dnf.partitions(arr);
        System.out.println("partitioned arr: " + Arrays.toString(arr));
    }
}

class Dnf {
    public static void partitions(int[] arr){
        int i=0;
        int j=0;
        int n=arr.length-1;
        int mid=getMid(arr);

        while (j<=n){
            if (arr[j] < mid){
                swap(arr, j, i);
                i++;
                j++;
            } else if (arr[j] > mid){
                swap(arr, j, n);
                n--;
            } else {
                j++;
            }
        }
    }

    private static void swap(int[] arr, int i, int j){
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    private static int getMid(int[] arr){
        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;
        for (int i=0; i<arr.length; i++){
            if (arr[i] < min) min = arr[i];
            if (arr[i] > max) max = arr[i];
        }
        return (min+max)/2;
    }
}

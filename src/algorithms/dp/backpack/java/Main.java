package algorithms.dp.backpack.java;

public class Main {
    public static void main(String[] args){
        int[][] weightValue = new int[][]{{22,10,9,7},{12,9,9,6}};
        System.out.println(BackPack.maxValue(weightValue, 25));
    }
}

class BackPack {

    public static int maxValue(int[][] weightValue, int W){
        int[] w = weightValue[0];
        int[] v = weightValue[1];
        int n = v.length;
        int[][] m = new int[n+1][W+1];

        for (int i=1; i<=n; i++){
            for (int j=0; j<=W; j++){
                if (w[i-1] > j){
                    m[i][j] = m[i-1][j];
                } else {
                    m[i][j] = Math.max(m[i-1][j-w[i-1]] + v[i-1], m[i-1][j]);
                }
            }
        }

        return m[n][W];
    }
}
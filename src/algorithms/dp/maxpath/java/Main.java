package algorithms.dp.maxpath.java;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {

    private static int[] buildArr(int n, int min, int max){
        int[] arr = new int[n];
        for (int i=0; i<n; i++){
            arr[i] = (int)(Math.random() * ((max - min) + 1)) + min;
        }
        return arr;
    }

    public static void main(String[] args){
        int[] arr = buildArr(1000000, 0, 1000);
        MaxPath mp = new MaxPath(arr);

        long startTime = System.currentTimeMillis();

        int maxSum = mp.getMaxSum();

        long stopTime = System.currentTimeMillis();
        long elapsedTime = stopTime - startTime;

        System.out.println(maxSum);
        System.out.println(mp.getMaxPath());
        System.out.println("time: " + elapsedTime);
    }
}

class MaxPath {

    private int[] binaryTree;
    private int[] maxSums;
    private int[] prev;
    private boolean maxSumsBuilt;

    public MaxPath(int[] binaryTree){
        this.binaryTree = binaryTree;
        this.maxSums = new int[binaryTree.length];
        this.prev = new int[binaryTree.length];
        Arrays.fill(prev, Integer.MIN_VALUE);
        this.maxSumsBuilt = false;
    }

    public List<Integer> getMaxPath(){
        checkBuildMaxSums();
        List<Integer> path = new ArrayList<>();
        int node = 0;
        while(prev[node] > Integer.MIN_VALUE){
            path.add(binaryTree[node]);
            node = prev[node];
        }
        path.add(binaryTree[node]);

        return path;
    }

    public int getMaxSum(){
        checkBuildMaxSums();
        if (binaryTree.length > 0){
            return maxSums[0];
        } else {
            return 0;
        }
    }

    private void checkBuildMaxSums(){
        if (maxSumsBuilt == false){
            if (binaryTree.length > 0){
                buildMaxSums(0);
                //buildMaxSumsIterative();
            }
            maxSumsBuilt = true;
        }
    }

    private void buildMaxSumsIterative(){
        for (int i=binaryTree.length-1; i>=0; i--){
            int left = 2*i + 1;
            int right = 2*i +2;
            if (left < binaryTree.length) {
                if (right < binaryTree.length) {
                    if (maxSums[left] >= maxSums[right]){
                        prev[i] = left;
                        maxSums[i] = binaryTree[i] + maxSums[left];
                    } else {
                        prev[i] = right;
                        maxSums[i] = binaryTree[i] + maxSums[right];
                    }

                } else {
                    prev[i] = left;
                    maxSums[i] = binaryTree[i] + maxSums[left];
                }
            } else {
                maxSums[i] = binaryTree[i];
            }
        }
    }

    private int buildMaxSums(int node){
        int left = 2*node + 1;
        int right = 2*node +2;

        if (left < binaryTree.length){
            if (right < binaryTree.length){
                int leftSum = buildMaxSums(left);
                int rightSum = buildMaxSums(right);
                if (leftSum >= rightSum){
                    prev[node] = left;
                    maxSums[node] = binaryTree[node] + leftSum;
                    return maxSums[node];
                } else {
                    prev[node] = right;
                    maxSums[node] = binaryTree[node] + rightSum;
                    return maxSums[node];
                }
            } else {
                prev[node] = left;
                maxSums[node] = binaryTree[node] + buildMaxSums(left);
                return maxSums[node];
            }
        } else {
            prev[node] = Integer.MIN_VALUE;
            maxSums[node] = binaryTree[node];
            return maxSums[node];
        }
    }

}

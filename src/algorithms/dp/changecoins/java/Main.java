package algorithms.dp.changecoins.java;

public class Main {
    public static void main(String[] args){
        int[] coins = new int[]{1,2,3};
        System.out.println(ChangeCoins.countRecursive(coins, coins.length, 4));
        System.out.println(ChangeCoins.countDp(coins, coins.length, 4));
    }
}

class ChangeCoins {
    public static int countRecursive(int[] coins, int m, int v){
        if (v==0) return 1;
        if (v<0) return 0;
        if (m<=0 && v > 0) return 0;

        return countRecursive(coins, m-1, v) + countRecursive(coins, m, v - coins[m-1]);
    }

    public static int countDp(int[] coins, int m, int v){
        int[] counts = new int[v+1];
        counts[0] = 1;
        for (int i=0; i<m; i++){
            for (int j=coins[i]; j<=v; j++){
                counts[j] += counts[j - coins[i]];
            }
        }
        return counts[v];
    }
}
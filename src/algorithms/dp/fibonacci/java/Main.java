package algorithms.dp.fibonacci.java;

import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args){

        long startTime = System.currentTimeMillis();

        //int nth = Fibonacci.recursiveFib(45); // exponential
        int nth = Fibonacci.dpFib(1000); // O(n)

        long stopTime = System.currentTimeMillis();
        long elapsedTime = stopTime - startTime;

        System.out.println(nth);
        System.out.println("time: " + elapsedTime);
    }
}

class Fibonacci {

    public static int recursiveFib(int n){
        if (n < 0) return -1;
        if (n == 0) return 0;
        if (n <= 2) return 1;
        return recursiveFib(n-1) + recursiveFib(n-2);
    }

    public static int dpFib(int n){
        if (n < 0) return -1;
        if (n == 0) return 0;

        Map<Integer, Integer> mem = new HashMap<>();
        return recursiveDpFib(mem, n);
    }

    private static int recursiveDpFib(Map<Integer, Integer> mem, int n){
        if (mem.containsKey(n)) return mem.get(n);

        int result;
        if (n <= 2) {
            result = 1;
        } else {
            result = recursiveDpFib(mem, n-1) + recursiveDpFib(mem, n-2);
        }
        mem.put(n, result);

        return result;
    }

}

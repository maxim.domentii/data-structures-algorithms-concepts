package algorithms.dp.mincoins.java;

public class Main {
    public static void main(String[] args){
        int[] coins = new int[]{1,5,6,9};
        System.out.println(MinCoins.recursive(coins, coins.length, 11));
        System.out.println(MinCoins.dp(coins, coins.length, 11));
    }
}

class MinCoins {
    public static int recursive(int[] coins, int m, int v){
        if (v==0) return 0;
        int res = Integer.MAX_VALUE;
        for (int i=0; i<m; i++){
            if (coins[i] <= v){
                int temp = 1 + recursive(coins, m, v-coins[i]);
                if (temp < res){
                    res = temp;
                }
            }

        }
        return res;
    }

    public static int dp(int[] coins, int m, int v){
        int[] minCoins = new int[v+1];
        minCoins[0] = 0;
        for (int i=1; i<=v; i++){
            minCoins[i] = Integer.MAX_VALUE;
            for (int j=0; j<m; j++){
                if (coins[j] <= i){
                    int temp = 1 + minCoins[i - coins[j]];
                    if (temp < minCoins[i]){
                        minCoins[i] = temp;
                    }
                }
            }
        }
        return minCoins[v];
    }
}
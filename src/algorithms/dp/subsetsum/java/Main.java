package algorithms.dp.subsetsum.java;

import java.util.*;

public class Main {

    private static List<Integer> buildList(int n, int min, int max){
        List<Integer> list = new LinkedList<>();
        for (int i=0; i<n; i++){
            list.add((int)(Math.random() * ((max - min) + 1)) + min);
        }
        return list;
    }

    public static void main(String[] args){
        //List<Integer> set = new ArrayList<>(Arrays.asList(1,6,2,7,3,8,4,20,5,9,21,22,23,24,25,26,27,28,29,10));
        List<Integer> set = buildList(40, -1000, 1000);

        long startTime = System.currentTimeMillis();

        System.out.println(SubSetSum.subSetSum(set, 1234));
        //System.out.println(SubSetSum.subSetSum(set, 15));

        long stopTime = System.currentTimeMillis();
        long elapsedTime = stopTime - startTime;
        System.out.println("time: " + elapsedTime);
    }
}

class SubSetSum {

    public static List<Integer> subSetSum(List<Integer> set, int n){
        if (set == null || set.isEmpty()){
            return null;
        }
        //return subSetSumRecursion(set, n);
        return subSetSumImproved(set, n);
    }

    //============

    private static List<Integer> subSetSumImproved(List<Integer> set, int n){
        List<Integer> result = new LinkedList<>();

        List<Integer> left = new ArrayList<>(set.subList(0, set.size()/2));
        TreeMap<Integer, List<Integer>> leftSumsTree = getSubSetsSums(getSubSets(left));
        List<Integer> leftSums = new ArrayList<>(leftSumsTree.keySet());

        List<Integer> right = new ArrayList<>(set.subList(set.size()/2, set.size()));
        TreeMap<Integer, List<Integer>> rightSumsTree = getSubSetsSums(getSubSets(right));
        List<Integer> rightSums = new ArrayList<>(rightSumsTree.keySet());

        int r = rightSums.size() - 1;
        int l = 0;
        while (r >= 0 && l < leftSums.size()){
            int sum = leftSums.get(l) + rightSums.get(r);
            if (sum == n) {
                result.addAll(leftSumsTree.get(leftSums.get(l)));
                result.addAll(rightSumsTree.get(rightSums.get(r)));
                return result;
            } else if (sum < n){
                l++;
            } else {
                r--;
            }
        }
        while (r >= 0){
            int sum = leftSums.get(l-1) + rightSums.get(r);
            if (sum == n) {
                result.addAll(leftSumsTree.get(leftSums.get(l-1)));
                result.addAll(rightSumsTree.get(rightSums.get(r)));
                return result;
            } else {
                r--;
            }

        }
        while (l < leftSums.size()){
            int sum = leftSums.get(l) + rightSums.get(r+1);
            if (sum == n) {
                result.addAll(leftSumsTree.get(leftSums.get(l)));
                result.addAll(rightSumsTree.get(rightSums.get(r+1)));
                return result;
            } else {
                l++;
            }
        }

        return null;
    }

    private static TreeMap<Integer, List<Integer>> getSubSetsSums(List<List<Integer>> subSets){
        TreeMap<Integer, List<Integer>> sums = new TreeMap<>();
        for (List subSet : subSets){
            sums.put(sum(subSet), subSet);
        }
        return sums;
    }

    private static int sum(List<Integer> set){
        if (set.isEmpty()) return 0;
        return set.get(0) + sum(set.subList(1, set.size()));
    }

    private static List<List<Integer>> getSubSets(List<Integer> set){
        if (set.isEmpty()){
            return Collections.singletonList(set);
        } else {
            List<List<Integer>> subSets = new LinkedList<>();

            Integer first = set.get(0);
            for (List<Integer> subSet : getSubSets(set.subList(1, set.size()))) {
                List<Integer> subListWithFirst = new LinkedList<>();
                subListWithFirst.add(first);
                subListWithFirst.addAll(subSet);
                subSets.add(subListWithFirst);
                subSets.add(subSet);
            }
            return subSets;
        }
    }

    //============

    private static List<Integer> subSetSumRecursion(List<Integer> set, int n){
        List<Integer> subSet = new LinkedList<>();
        if (n==0) return subSet;
        if (n<0) return null;
        for (Integer i : set){
            subSet.add(i);
            List<Integer> temp = new LinkedList<>(set);
            temp.remove(i);
            temp = subSetSumRecursion(temp, n-i);
            if (temp != null){
                subSet.addAll(temp);
                return subSet;
            } else {
                subSet.remove(i);
            }
        }
        return null;
    }
}
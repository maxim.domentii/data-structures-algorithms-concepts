class SubSetSum {

    public List<Integer> subSetSum(List<Integer> set, int n){
        if (set == null || set.isEmpty()){
            return null;
        }
        return subSetSumRecursion(set, n);
    }

    private List<Integer> subSetSumRecursion(List<Integer> set, int n){
        List<Integer> subSet = new ArrayList<>();
        if (n==0) return subSet;
        if (n<0) return null;
        for (Integer i : set){
            subSet.add(i);
            set.remove(i);
            List<Integer> result = subSetSumRecursion(set, n-i);
            if (result != null){
                subSet.addAll(result);
                return subSet;
            }
        }
        return null;
    }
}
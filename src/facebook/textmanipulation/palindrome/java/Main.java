package facebook.textmanipulation.palindrome.java;

public class Main {
    public static void main(String[] args){
        System.out.println(Palindrome.isPalindrome("Was it a car or a cat I saw"));
    }
}

class Palindrome {
    public static boolean isPalindrome(String s){
        s = s.toLowerCase();
        int i=0;
        int j=s.length() - 1;
        while (i<j){
            if (s.charAt(i) == ' '){
                i++;
                continue;
            }
            if (s.charAt(j) == ' '){
                j--;
                continue;
            }
            if (s.charAt(i) != s.charAt(j)) return false;
            i++;
            j--;
        }
        return true;
    }
}
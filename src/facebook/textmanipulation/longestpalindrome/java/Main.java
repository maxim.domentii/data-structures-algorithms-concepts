package facebook.textmanipulation.longestpalindrome.java;

public class Main {
    public static void main(String[] args){
        System.out.println(LongestPalindrome.getMaxPalindromeLength("abaccddccefffe"));
    }
}

class LongestPalindrome {
    public static int getMaxPalindromeLength(String s){
        if (s.isEmpty()){
            return 0;
        }
        return dp(s);//recursion(s, -1);
    }

    private static int recursion(String s, int max){
        if (s.length() == 1){
            max = Math.max(1, max);
            return max;
        }

        for (int i=0; i<s.length(); i++){
            String temp = s.substring(i);
            if (Palindrome.isPalindrome(temp)){
                max = Math.max(temp.length(), max);
            } else {
                max = recursion(temp.substring(0, temp.length()-1), max);
            }
        }
        return max;
    }

    private static int dp(String s){
        int n = s.length();
        boolean[][] table = new boolean[n][n];

        int max = 1;
        for (int i = 0; i< n; i++){
            table[i][i] = true;
        }

        for (int i = 0; i< n - 1; i++){
            if (s.charAt(i) == s.charAt(i+1)){
                table[i][i+1] = true;
                max = 2;
            }
        }

        for (int k = 3; k<n; k++){
            for (int i=0; i<n-k+1; i++){
                int j = i+k-1;
                if (table[i+1][j-1] && s.charAt(i) == s.charAt(j)){
                    table[i][j] = true;
                    if (k > max) max = k;
                }
            }
        }

        return max;
    }

}

class Palindrome {
    public static boolean isPalindrome(String s){
        s = s.toLowerCase();
        int i=0;
        int j=s.length() - 1;
        while (i<j){
            if (s.charAt(i) == ' '){
                i++;
                continue;
            }
            if (s.charAt(j) == ' '){
                j--;
                continue;
            }
            if (s.charAt(i) != s.charAt(j)) return false;
            i++;
            j--;
        }
        return true;
    }
}
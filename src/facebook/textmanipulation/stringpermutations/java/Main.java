package facebook.textmanipulation.stringpermutations.java;

import java.util.*;

public class Main {
    public static void main(String[] args){
        System.out.println(StringPermutations.getPermutations("ABC"));
    }
}

class StringPermutations {

    public static Set<String> getPermutations(String s){
        s = removeDublicates(s);
        if (s.length() < 2){
            return new TreeSet<>(Arrays.asList(s));
        }

        Set<String> result = new TreeSet<>();

        recursion(s.toCharArray(), result, s.length());

        return result;
    }

    private static List<String> recursion(char[] s, Set<String> result, int size){

        if (s.length == 1){
            return Arrays.asList(s[0] + "");
        }

        List<String> permutations = new ArrayList<>();
        for (int i=0; i<s.length; i++) {
            char[] temp = removeCharFromArray(s, s[i]);
            List<String> currentPermutations = recursion(temp, result, size);
            for (String currentPermutation : currentPermutations){
                String permutation = s[i] + currentPermutation;
                permutations.add(permutation);
                if (permutation.length() == size){
                    result.add(permutation);
                }
            }

        }

        return permutations;
    }

    private static char[] removeCharFromArray(char[] array, char c){
        char[] result = new char[array.length - 1];
        int resultIndex = 0;
        boolean found = false;
        for (int i=0; i< array.length; i++){
            if (c != array[i] || found == true){
                result[resultIndex++] = array[i];
            } else {
                found = true;
            }
        }
        return result;
    }

    private static String removeDublicates(String s){
        Set<Character> set = new TreeSet<>();
        for (int i=0; i<s.length(); i++){
            set.add(s.charAt(i));
        }
        StringBuilder sb = new StringBuilder();
        for (char c : set){
            sb.append(c);
        }
        return sb.toString();
    }
}
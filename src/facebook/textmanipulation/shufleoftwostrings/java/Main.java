package facebook.textmanipulation.shufleoftwostrings.java;

public class Main {
    public static void main(String[] args){
        System.out.println(ShufleOfTwoStrings.isValid("ABCD", "AB", "CD"));
    }
}

class ShufleOfTwoStrings {
    public static boolean isValid(String interleaving, String s1, String s2){
        int i=0;
        int j=0;
        int k=0;

        if (interleaving.length() != s1.length() + s2.length()){
            return false;
        }

        while (k<interleaving.length()){
            if (i < s1.length() && interleaving.charAt(k) == s1.charAt(i)){
                i++;
                k++;
            } else if (j < s2.length() && interleaving.charAt(k) == s2.charAt(j)){
                j++;
                k++;
            } else {
                return false;
            }
            if (i > s1.length() && j > s2.length() && k<interleaving.length()){
                return false;
            }
        }
        return true;
    }
}

package facebook.textmanipulation.sortstrings.java;

import java.util.Arrays;
import java.util.Comparator;

public class Main {
    public static void main(String[] args){
        String[] strings = new String[]{"abc", "abcd", "ab", "a", "ba", "cba", "dcba"};
        SortStrings.sort1(strings);
        System.out.println(Arrays.toString(strings));

        strings = new String[]{"abc", "abcd", "ab", "a", "ba", "cba", "dcba"};
        SortStrings.sort2(strings);
        System.out.println(Arrays.toString(strings));
    }
}


class SortStrings {

    public static void sort1(String[] strings){
        recursion(strings, 0, strings.length-1);
    }

    private static void recursion(String[] strings, int start, int end){
        if (end - start > 1){
            int p = partitioning(strings, start, end);
            recursion(strings, start, p);
            recursion(strings, p+1, end);
        }
    }

    private static int partitioning(String[] strings, int start, int end){
        int mid = strings[(start + end)/2].length();
        while (true){
            while (strings[start].length() <= mid){
                start++;
            }
            while (strings[end].length() > mid){
                end--;
            }
            if (start >= end){
                return end;
            }
            String temp = strings[start];
            strings[start] = strings[end];
            strings[end] = temp;
        }
    }

    public static void sort2(String[] strings){
        Comparator<String> c = new Comparator<String>(){
            @Override
            public int compare(String s1, String s2) {
                return s1.length() <= s2.length() ? (s1.length() == s2.length() ? 0 : -1) : 1;
            }
        };

        Arrays.sort(strings, c);
    }

}
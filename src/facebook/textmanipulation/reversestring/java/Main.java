package facebook.textmanipulation.reversestring.java;

public class Main {
    public static void main(String[] args){
        System.out.println(ReverseString.reverseIterative("ala bala portocala"));
        System.out.println(ReverseString.reverseRecursive("ala bala portocala"));
    }
}

class ReverseString {

    public static String reverseIterative(String s){
        StringBuilder sb = new StringBuilder();
        for (int i=s.length()-1; i>=0; i--){
            sb.append(s.charAt(i));
        }
        return sb.toString();
    }

    public static String reverseRecursive(String s){
        if (s.isEmpty()) return "";
        return s.substring(s.length()-1, s.length()) + reverseRecursive(s.substring(0, s.length()-1));
    }
}

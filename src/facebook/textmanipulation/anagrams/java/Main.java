package facebook.textmanipulation.anagrams.java;

import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args){
        System.out.println(Anagrams.areAnagrams("William Shakespeare", "I am a weakish speller"));
    }
}

class Anagrams {

    public static boolean areAnagrams(String s1, String s2){
        s1 = s1.toLowerCase();
        s2 = s2.toLowerCase();
        Map<Character, Integer> lettersCountFromS1 = new HashMap<>();

        char[] charsOfS1 = s1.toCharArray();
        for (int i=0; i<charsOfS1.length; i++){
            char currentChar = charsOfS1[i];
            if (currentChar == ' '){
                continue;
            }

            int count = 0;
            if (lettersCountFromS1.containsKey(currentChar)){
                count = lettersCountFromS1.get(currentChar);
            }
            lettersCountFromS1.put(currentChar, count+1);
        }

        char[] charsOfS2 = s2.toCharArray();
        for (int i=0; i<charsOfS2.length; i++){
            char currentChar = charsOfS2[i];
            if (currentChar == ' '){
                continue;
            }
            if (lettersCountFromS1.containsKey(currentChar) && lettersCountFromS1.get(currentChar) > 0){
                if (lettersCountFromS1.get(currentChar) == 1){
                    lettersCountFromS1.remove(currentChar);
                } else {
                    lettersCountFromS1.put(currentChar, lettersCountFromS1.get(currentChar)-1);
                }
            } else {
                return false;
            }
        }

        if (lettersCountFromS1.size() > 0){
            return false;
        }

        return true;
    }

}
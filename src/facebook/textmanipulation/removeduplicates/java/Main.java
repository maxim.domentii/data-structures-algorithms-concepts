package facebook.textmanipulation.removeduplicates.java;

import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args){
        System.out.println(RemoveDuplicates.removeDuplicates("ala bala portocala"));
    }
}

class RemoveDuplicates {
    public static String removeDuplicates(String s){
        Map<Character, Integer> map = new HashMap<>();
        StringBuilder sb = new StringBuilder();
        for (int i=0; i<s.length(); i++){
            char c = s.charAt(i);
            if (!map.containsKey(c)){
                if (c != ' '){
                    map.put(c, 1);
                }
                sb.append(c);
            }
        }

        return sb.toString();
    }
}

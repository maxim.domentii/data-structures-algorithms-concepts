package facebook.textmanipulation.printdublicatedchars.java;

public class Main {
    public static void main(String[] args){
        System.out.println(PrintDuplicatedChars.getDuplicatedChars("ala bala portocala"));
    }
}

class PrintDuplicatedChars {

    private static final int NUM_OF_CHARS = 256;

    public static String getDuplicatedChars(String s){
        int[] count = new int[NUM_OF_CHARS];
        char[] chars = s.toCharArray();
        for (int i=0; i<chars.length; i++){
            count[chars[i]]++;
        }

        StringBuilder sb = new StringBuilder();
        sb.append("{");
        for (int i=0; i<NUM_OF_CHARS; i++){
            if (count[i] > 1){
                sb.append((char)i + ", ");
            }
        }

        sb.replace(sb.length()-2, sb.length(), "");
        sb.append("}");
        return sb.toString();
    }
}

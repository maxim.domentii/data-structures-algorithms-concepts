package facebook.textmanipulation.printfirstnonrepeatedchar.java;

import java.util.LinkedHashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args){
        System.out.println(FirstNonRepeatedChar.getFirstNonRepeatedChar("ala bala portocala"));
    }
}

class FirstNonRepeatedChar {

    public static char getFirstNonRepeatedChar(String s){
        s = s.toLowerCase();
        char[] chars = s.toCharArray();

        Map<Character, Integer> map = new LinkedHashMap<>();
        for (int i=0; i<chars.length; i++){
            char currentChar = chars[i];
            if (map.containsKey(currentChar)){
                map.put(currentChar, 1);
            } else {
                map.put(currentChar, 0);
            }
        }

        for (Map.Entry<Character, Integer> entry : map.entrySet()){
            if (entry.getValue() == 0){
                return entry.getKey();
            }
        }

        return ' ';
    }

}

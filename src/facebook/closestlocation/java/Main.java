package facebook.closestlocation.java;

public class Main {
    public static void main(String[] args){
        ClosestLocation.Point[] locations = new ClosestLocation.Point[]{
                new ClosestLocation.Point(6,6),
                new ClosestLocation.Point(3,6),
                new ClosestLocation.Point(4,3),
                new ClosestLocation.Point(1,3),
                new ClosestLocation.Point(2,1),
        };

        System.out.println(ClosestLocation.getClosest(locations, new ClosestLocation.Point(0,0)));
        System.out.println(ClosestLocation.getClosestWithRadixSort(locations));
    }
}

class ClosestLocation {

    public static Point getClosestWithRadixSort(Point[] locations){
        if (locations == null || locations.length == 0){
            return null;
        }

        locations = sortByCoordinate(locations, true);
        locations = sortByCoordinate(locations, false);

        return locations[0];
    }

    private static Point[] sortByCoordinate(Point[] locations, boolean sortByX){
        Point[] output = new Point[locations.length];
        int[] count = new int[getMaxCoordinate(locations, sortByX)+1];

        for (int i=0; i<locations.length; i++){
            if (sortByX) {
                count[locations[i].x]++;
            } else {
                count[locations[i].y]++;
            }
        }

        for (int i=1; i<count.length; i++){
            count[i] += count[i-1];
        }

        for (int i=locations.length-1; i>=0; i--){
            if (sortByX){
                output[count[locations[i].x] - 1] = locations[i];
                count[locations[i].x]--;
            } else {
                output[count[locations[i].y] - 1] = locations[i];
                count[locations[i].y]--;
            }
        }

        return output;
    }

    private static int getMaxCoordinate(Point[] locations, boolean coordinateX){
        int max = Integer.MIN_VALUE;

        for (int i=0; i<locations.length; i++){
            if (coordinateX){
                if (locations[i].x > max) max = locations[i].x;
            } else {
                if (locations[i].y > max) max = locations[i].y;
            }
        }

        return max;
    }

    public static Point getClosest(Point[] locations, Point currentLocation){
        double minDistance = Double.MAX_VALUE;
        Point closest = null;

        for (int i=0 ; i<locations.length; i++){
            double d = distance(currentLocation, locations[i]);
            if (d < minDistance) {
                minDistance = d;
                closest = locations[i];
            }
        }

        return closest;
    }

    private static double distance(Point p1, Point p2){
        return Math.sqrt(Math.pow(Math.abs(p2.x-p1.x), 2) + Math.pow(Math.abs(p2.y-p1.y), 2));
    }

    public static class Point {
        int x;
        int y;

        public Point(int x, int y){
            this.x=x;
            this.y=y;
        }

        @Override
        public String toString() {
            return "{ " + x + ", " + y + "}";
        }
    }

}

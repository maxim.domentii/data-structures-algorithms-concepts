package facebook.partitioninthreeranges.java;

import java.util.Arrays;

public class Main {
    public static void main(String[] args){
        int[] arr = new int[]{1, 14, 5, 20, 4, 2, 54, 20, 87, 98, 3, 1, 32, 6, 32};
        System.out.println("initial arr: " + Arrays.toString(arr));

        System.out.println("partitioned arr: " + Arrays.toString(PartitionInThreeRanges.partition(arr, 6,32)));
    }
}

class PartitionInThreeRanges {
    public static int[] partition(int[] arr, int low, int high){
        int[] output = new int[arr.length];

        int[] count = new int[3];
        for (int i=0; i< arr.length; i++){
            if (arr[i] < low) {
                count[0]++;
            }
            if (arr[i] >= low && arr[i] <= high) {
                count[1]++;
            }
            if (arr[i] > high) {
                count[2]++;
            }
        }
        for (int i=1; i<count.length; i++){
            count[i] += count[i-1];
        }

        for (int i=arr.length-1; i>=0; i--){
            if (arr[i] < low) {
                output[count[0]-1] = arr[i];
                count[0]--;
            }
            if (arr[i] >= low && arr[i] <= high) {
                output[count[1]-1] = arr[i];
                count[1]--;
            }
            if (arr[i] > high) {
                output[count[2]-1] = arr[i];
                count[2]--;
            }
        }

        return output;
    }
}

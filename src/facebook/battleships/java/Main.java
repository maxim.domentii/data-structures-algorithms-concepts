package facebook.battleships.java;

import java.util.Arrays;

public class Main {
    public static void main(String[] args){
        int[][] table = new int[][]{
                {1,0,0,0,0},
                {2,2,0,0,0},
                {3,3,3,0,0},
                {4,4,4,4,0},
                {5,5,5,5,5}
        };

        int[] ships = new int[]{0, 1, 2, 3, 4, 5};

        Battleships battleships = new Battleships(table, ships, 5);
        System.out.println("Initial table: \n" + battleships.tableToString());
        System.out.println("Game finished. Fire called " + battleships.play() + " times");

    }
}

class Battleships {

    private int[][] table;
    private int[] ships;
    private int fireNum;
    private int countFireCalls = 0;

    public Battleships(int[][] table, int[] ships, int fireNum) {
        this.table = table;
        this.ships = ships;
        this.fireNum = fireNum;
    }

    // fire method
    private int[] fire(int[][] coords){
        int[] xs = coords[0];
        int[] ys = coords[1];

        for (int i=0; i<this.fireNum; i++){
            int cell = table[xs[i]][ys[i]];
            if (cell > 0){
                table[xs[i]][ys[i]] = 0 - cell;
                this.ships[cell]--;
            }
        }
        this.countFireCalls++;

        return this.ships;
    }

    public String tableToString(){
        StringBuilder sb = new StringBuilder();
        for (int i=0; i<this.table.length; i++){
            sb.append(Arrays.toString(table[i]) + "\n");
        }
        return sb.toString();
    }

    // check if game is over
    private boolean gameOver(){
        for (int i=1; i<this.ships.length; i++){
            if (this.ships[i] > 0){
                return false;
            }
        }
        return true;
    }

    // play method which calls fire minimum times until kill all ships
    public int play() {

        int[] startPos = new int[]{0,0};
        while (!gameOver()){
            startPos = callFire(this.table.length, this.fireNum, startPos);
            System.out.println("Table after fire " + this.countFireCalls + ":\n" + tableToString());
            if (startPos[0] == -1){
                return -1;
            }
        }

        if (gameOver()){
            return this.countFireCalls;
        } else {
            return -1;
        }
    }

    private int[] callFire(int size, int fireNum, int[] startPos){
        int x = startPos[0];
        int y = startPos[1];

        int[][] fireInput = new int[2][fireNum];
        for (; x<size; x++){
            if (y == size){
                y = 0;
            }
            for (; y<size; y++){
                if (fireNum == 0) {
                    fire(fireInput);
                    return new int[]{x,y};
                }
                fireInput[0][fireNum-1] = x;
                fireInput[1][fireNum-1] = y;
                fireNum--;
            }
        }
        if (x == size && y == size){
            fire(fireInput);
            return new int[]{x,y};
        }

        return new int[]{-1,-1};
    }
}

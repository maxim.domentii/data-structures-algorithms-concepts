package facebook.mergefiles.java;

import java.io.*;

public class Main {
    public static void main(String[] args) throws IOException {
        MergeFailes.merge("input1.txt", "input2.txt", "output.txt");
    }
}

class MergeFailes {
    public static void merge(String inputFile1, String inputFile2, String outputFile) throws IOException {
        PrintWriter writer = new PrintWriter(outputFile);
        BufferedReader reader1 = new BufferedReader(new FileReader(inputFile1));
        BufferedReader reader2 = new BufferedReader(new FileReader(inputFile2));

        String line1 = reader1.readLine();
        String line2 = reader2.readLine();
        while (line1 != null || line2 != null){
            if (line1 != null){
                writer.println(line1);
                line1 = reader1.readLine();
            }
            if (line2 != null){
                writer.println(line2);
                line2 = reader2.readLine();
            }
        }

        writer.close();
        reader1.close();
        reader2.close();
    }
}

// reversing a linked list using recursion and non-recursion method
class LinkedList<T> {

    private Node<T> first;
    private Node<T> last;
    private int size;

    public LinkedList() {
        this.first = this.last = null;
        this.size = 0;
    }

    public void reverseIterative(){
        if (first == null || first == last){
            return;
        }

        Node<T> temp = first.next;
        last = new Node(first.value, null);
        first = last;
        while (temp != null){
            Node<T> newNode = new Node(temp.value, first);
            first.next = newNode;
            temp = temp.next;
        }

    }

    public void reverseRecursive() {
        if (first == null || first == last){
            return;
        }

        Node<T> temp = first.next;
        last = new Node(first.value, null);
        first = last;
        recursion(temp);
    }

    private void recursion(Node<T> node){
        if (node == null){
            return;
        }
        Node<T> newNode = new Node(node.value, first);
        first.next = newNode;
        recursion(node.next);
    }

    public void addAll(Collection<T> col){
        for (T elem : col){
            add(elem);
        }
    }

    public void add(T value){
        if (first == null){
            first = last = new Node(value, null);
        } else {
            Node<T> newNode = new Node(value, null);
            last.next = newNode;
            last = newNode;
        }
        size++;
    }

    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("[");

        Node<T> temp = first;
        while (temp != null){
            sb.append(temp.value);
            if (temp.next != null){
                sb.append(", ");
            }
            temp = temp.next;
        }
        sb.append("]");
        return sb.toString();
    }

    public static class Node<T> {
        T value;
        Node next;

        public Node(T value, Node next) {
            this.value = value;
            this.next = next;
        }
    }
}
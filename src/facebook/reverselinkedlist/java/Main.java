package facebook.reverselinkedlist.java;

import java.util.Arrays;
import java.util.Collection;

public class Main {
    public static void main(String[] args){
        LinkedList<Integer> list = new LinkedList<>();
        list.addAll(Arrays.asList(1,2,3,4,5,6,7,8,9));
        System.out.println("initial list: " + list);

        list.reverseIterative();
        System.out.println("list reversed iteratively: " + list);

        list.reverseRecursive();
        System.out.println("list reversed again recursively: " + list);

    }
}

class LinkedList<T> {

    private Node<T> first;
    private Node<T> last;

    public LinkedList() {
        this.first = this.last = null;
    }

    public void reverseIterative(){
        if (first == null || first == last){
            return;
        }

        Node<T> temp = first.next;
        last = new Node(first.value, null);
        first = last;
        while (temp != null){
            Node<T> newNode = new Node(temp.value, first);
            first = newNode;
            temp = temp.next;
        }

    }

    public void reverseRecursive() {
        if (first == null || first == last){
            return;
        }

        Node<T> temp = first.next;
        last = new Node(first.value, null);
        first = last;
        recursion(temp);
    }

    private void recursion(Node<T> node){
        if (node == null){
            return;
        }
        Node<T> newNode = new Node(node.value, first);
        first = newNode;
        recursion(node.next);
    }

    public void addAll(Collection<T> col){
        for (T elem : col){
            add(elem);
        }
    }

    public void add(T value){
        if (first == null){
            first = last = new Node(value, null);
        } else {
            Node<T> newNode = new Node(value, null);
            last.next = newNode;
            last = newNode;
        }
    }

    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("[");

        Node<T> temp = first;
        while (temp != null){
            sb.append(temp.value);
            if (temp.next != null){
                sb.append(", ");
            }
            temp = temp.next;
        }
        sb.append("]");
        return sb.toString();
    }

    public static class Node<T> {
        T value;
        Node next;

        public Node(T value, Node next) {
            this.value = value;
            this.next = next;
        }
    }
}

package facebook.partitioninthreeblockwithmaxminsum.java;

public class Main {
    public static void main(String[] args){
        int[] arr = new int[]{2,80,50,42,1,1,1,2};
        System.out.println(PartitionInThreeBlockWithMaxMinSum.maxMinSum(arr));
    }
}

class PartitionInThreeBlockWithMaxMinSum {

    public static int maxMinSum(int[] arr){
        int result = Integer.MAX_VALUE;
        int i=0;
        int j=arr.length-1;
        int s1=0;
        int s3=0;

        int s2=0;
        for (int k=0;k<arr.length;k++){
            s2 += arr[k];
        }

        while (s1 < s2 && s3 < s2){
            if (s1 <= s3) {
                s1 += arr[i];
                s2 -= arr[i];
                i++;
            } else {
                s3 += arr[j];
                s2 -= arr[j];
                j--;
            }
            result = Math.max(Math.max(s1,s2), s3);
        }

        return result;
    }

}

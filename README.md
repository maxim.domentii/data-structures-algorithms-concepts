Software Engineer must-have
============================



| **Data Structures** | **Algorithms** | **Concepts** |
|---------------------|----------------|--------------|
|Arrays |Breadth First Search|Bit Manipulation|
|Linked Lists |Depth First Search| Singleton Design Pattern|
|Binary Trees |Binary Search |Factory Design Pattern|
|Tries |Merge Sort |Memory (Stack vs Heap)|
|Stacks |Quick Sort |Recursion |
|Queues |Bucket Sort |Big-O Time|
|Vectors / ArrayLists |Heap sort |Randomized quicksort|
|Hash Tables |Radix sort |Dynamic programming|
|Heap trees |Tree Insert / Find / etc |Spanning tree|
|Set trees |    |Minimum cuts|
|Red-black trees |  |   |